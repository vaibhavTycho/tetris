using System.Collections;
using System.Collections.Generic;
using TetrisEngine;
using UnityEngine;

public class MoveButtons_Tetris : MonoBehaviour
{
    [SerializeField] private GameLogic_Tetris gameLogic;
    public void LeftDir()
    {
        gameLogic.LeftDir();
    }
    public void RighttDir()
    {
        gameLogic.RighttDir();
    }
    public void DownDir()
    {
        gameLogic.DownDir();
    }
    public void LeftRotate()
    {
        gameLogic.LeftRotate();
    }
    public void Rightrotate()
    {
        gameLogic.Rightrotate();
    }
}
