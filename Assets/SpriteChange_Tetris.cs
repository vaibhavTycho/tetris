using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChange_Tetris : MonoBehaviour
{
    [SerializeField] private Sprite red, skyblue, green, purple, pink,demo;
    // [SerializeField] private SpriteRenderer 
    public void FillSprite(int i)
    {
        switch (i) {
            case 1:
               //  transform.localScale =new  Vector3(.4f,.4f,.4f);
                transform.GetComponent<SpriteRenderer>().sprite = red;
                transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                break;
            case 2:
              //  transform.localScale =new Vector3(.4f, .4f, .4f);
                transform.GetComponent<SpriteRenderer>().sprite= skyblue;
                transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                break;
            case 3:
               // transform.localScale =new Vector3(.4f, .4f, .4f);
                transform.GetComponent<SpriteRenderer>().sprite= green;
                transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                break;
            case 4:
              //  transform.localScale =new Vector3(.4f, .4f, .4f);
                transform.GetComponent<SpriteRenderer>().sprite= purple;
                transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                break;
            case 5:
              //  transform.localScale = new Vector3(.4f, .4f, .4f);
                transform.GetComponent<SpriteRenderer>().sprite= pink;
                transform.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
                break;
            case 6:
               //  transform.localScale =new  Vector3(1,1,1);
                 transform.GetComponent<SpriteRenderer>().sprite= demo;
                transform.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0.5f);
                break;

            }
    }
}
