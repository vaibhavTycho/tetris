using System.Collections;
using System.Collections.Generic;
using TetrisEngine;
using UnityEngine;

public class BackBlock_Tetris : MonoBehaviour
{
    [SerializeField] private SpriteRenderer block;
    private void Start()
    { 
        for(int i = 0; i < Playfield_Tetris.WIDTH; i++)
        {
            for(int j= 2; j>-Playfield_Tetris.HEIGHT; j--)
            {
                Instantiate(block, new Vector3(i ,j, 0), Quaternion.identity,gameObject.transform);
            }
        }
    }
}
