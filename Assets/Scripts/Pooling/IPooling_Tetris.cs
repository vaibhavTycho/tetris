﻿namespace pooling
{
    public interface IPooling_Tetris
    {
		string objectName { get; }
		bool isUsing { get; set; }
        void OnCollect();
        void OnRelease();
    }
}