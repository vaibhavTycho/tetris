using System.Collections;
using System.Collections.Generic;
using TetrisEngine;
using UnityEngine;

public class MainMenu_Tetris : ScoreScreenType_Tetris<MainMenu_Tetris>
{
	[SerializeField] private GameLogic_Tetris gameLogic;
   
	public override void ShowScreen(float timeToTween = TIME_TO_TWEEN)
	{
       // SetScoreText(Score.instance.PlayerScore);
      
		base.ShowScreen(timeToTween);
	}
    public override void HideScreen(float timeToTween = 1)
    {
        base.HideScreen(timeToTween);
        gameObject.SetActive(false);
    }
    public void OnPlayBtnClick()
    {
        Debug.Log("OnPlayClick");
        HideScreen();
		gameLogic.GameStart();
    }
}