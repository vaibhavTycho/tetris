﻿using UnityEngine;
using TetrisEngine.TetriminosPiece;
using System.Collections.Generic;
using pooling;
using UnityEngine.UI;

namespace TetrisEngine
{   
	//This class is responsable for conecting the engine to the view
    //It is also responsable for calling Playfield.Step
	public class GameLogic_Tetris : MonoBehaviour 
    {
		private const string JSON_PATH = @"SupportFiles/GameSettings";

		public GameObject tetriminoBlockPrefab;
		public Transform tetriminoParent;
		[HideInInspector]public bool moveDownfast=false;
        [Header("This property will be overriten by GameSettings.json file.")] 
		[Space(-10)]
		[Header("You can play with it while the game is in Play-Mode.")] 
		public float timeToStep = 2f;

		private GameSettings_Tetris mGameSettings;
		private Playfield_Tetris mPlayfield;
		private List<TetriminoView_Tetris> mTetriminos = new List<TetriminoView_Tetris>();
		private float mTimer = 0f;
        
		private Pooling_Tetris<TetriminoBlock_Tetris> mBlockPool = new Pooling_Tetris<TetriminoBlock_Tetris>();    
		private Pooling_Tetris<TetriminoView_Tetris> mTetriminoPool = new Pooling_Tetris<TetriminoView_Tetris>();

		private Tetrimino_Tetris mCurrentTetrimino
		{
			get
			{
				return (mTetriminos.Count > 0 && !mTetriminos[mTetriminos.Count - 1].isLocked) ? mTetriminos[mTetriminos.Count - 1].currentTetrimino : null;
			}
		}

		private TetriminoView_Tetris mPreview;
		private bool mRefreshPreview;
		private bool mGameIsOver;
		[SerializeField] private GameObject swipeHandler;
        //Regular Unity Start method
        //Responsable for initiating all the pooling systems and the playfield
		public void Start()
		{
			swipeHandler.SetActive(false);
			Score_Tetris.instance.UpdateHighScore();
			moveDownfast = false;
			mBlockPool.createMoreIfNeeded = true;
			mBlockPool.Initialize(tetriminoBlockPrefab, null);
			//mBlockPool.Initialize(1, tetriminoBlockPrefab, null,new Vector3(2,-5,0)) ;
			mTetriminoPool.createMoreIfNeeded = true;
			mTetriminoPool.Initialize(new GameObject("BlockHolder", typeof(RectTransform)), tetriminoParent);
			mTetriminoPool.OnObjectCreationCallBack += x =>
			{
				x.OnDestroyTetrimoView = DestroyTetrimino;
				x.blockPool = mBlockPool;
			};

			//Checks for the json file
			var settingsFile = Resources.Load<TextAsset>(JSON_PATH);
			if (settingsFile == null)
				throw new System.Exception(string.Format("GameSettings.json could not be found inside {0}. Create one in Window>GameSettings Creator.", JSON_PATH));

			//Loads the GameSettings Json
			var json = settingsFile.text;
			mGameSettings = JsonUtility.FromJson<GameSettings_Tetris>(json);
			mGameSettings.CheckValidSettings();
			timeToStep = mGameSettings.timeToStep;

			mPlayfield = new Playfield_Tetris(mGameSettings);
			mPlayfield.OnCurrentPieceReachBottom = CreateTetrimino;
			mPlayfield.OnGameOver = SetGameOver;
			mPlayfield.OnDestroyLine = DestroyLine;

			GameOver_Tetris.instance.HideScreen(0f);
			Score_Tetris.instance.HideScreen();
			mGameIsOver = true;
		
		}
		public void GameStart()
        {
			RestartGame();
		}
        //Called when the game starts and when user click Restart Game on GameOver screen
        //Responsable for restaring all necessary components
        public void RestartGame()
		{
			GameOver_Tetris.instance.HideScreen();
			Score_Tetris.instance.ResetScore();
			moveDownfast = false;

			mGameIsOver = false;
			mTimer = 0f;
            
			mPlayfield.ResetGame();
			mTetriminoPool.ReleaseAll();
			mTetriminos.Clear();

            CreateTetrimino();  
			swipeHandler.SetActive(true);

		}

		//Callback from Playfield to destroy a line in view
		private void DestroyLine(int y)
		{
			Score_Tetris.instance.AddPoints(mGameSettings.pointsByBreakingLine);
            
			mTetriminos.ForEach(x => x.DestroyLine(y));
            mTetriminos.RemoveAll(x => x.destroyed == true);
		}

        //Callback from Playfield to show game over in view
		private void SetGameOver()
		{
			Debug.Log("GameOver ");
			mGameIsOver = true;

			GameOver_Tetris.instance.ShowScreen();
			swipeHandler.SetActive(false);

		}

		//Call to the engine to create a new piece and create a representation of the random piece in view
		private void CreateTetrimino()
		{
			if (mCurrentTetrimino != null)
				mCurrentTetrimino.isLocked = true;
			
			var tetrimino = mPlayfield.CreateTetrimo();
			var tetriminoView = mTetriminoPool.Collect();         
			tetriminoView.InitiateTetrimino(tetrimino);
			mTetriminos.Add(tetriminoView);

			if (mPreview != null)
				mTetriminoPool.Release(mPreview);
			
			mPreview = mTetriminoPool.Collect();
			mPreview.InitiateTetrimino(tetrimino, true);
			mRefreshPreview = true;
		}

		//When all the blocks of a piece is destroyed, we must release ("destroy") it.
		private void DestroyTetrimino(TetriminoView_Tetris obj)
		{
			var index = mTetriminos.FindIndex(x => x == obj);
			mTetriminoPool.Release(obj);
			mTetriminos[index].destroyed = true;
		}

		//Regular Unity Update method
        //Responsable for counting down and calling Step
        //Also responsable for gathering users input
		public void Update()
		{
			if (mGameIsOver) return;

			mTimer += Time.deltaTime;
			if(mTimer > timeToStep)
			{
				mTimer = 0;
				mPlayfield.Step();
			}

			if (mCurrentTetrimino == null) return;

            //Rotate Right
			if(Input.GetKeyDown(mGameSettings.rotateRightKey))
			{
				if(mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
    											  mCurrentTetrimino.currentPosition.y,
    											  mCurrentTetrimino,
    			                                  mCurrentTetrimino.NextRotation))
				{
					mCurrentTetrimino.currentRotation = mCurrentTetrimino.NextRotation;
					mRefreshPreview = true;
				}
			}

			//Rotate Left
			if (Input.GetKeyDown(mGameSettings.rotateLeftKey))
            {
                if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
                                                  mCurrentTetrimino.currentPosition.y,
                                                  mCurrentTetrimino,
    			                                  mCurrentTetrimino.PreviousRotation))
                {
					mCurrentTetrimino.currentRotation = mCurrentTetrimino.PreviousRotation;
					mRefreshPreview = true;
                }
            }

            //Move piece to the left
			if (Input.GetKeyDown(mGameSettings.moveLeftKey))
            {
                if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x - 1,
                                                  mCurrentTetrimino.currentPosition.y,
                                                  mCurrentTetrimino,
                                                  mCurrentTetrimino.currentRotation))
                {
                    mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x - 1, mCurrentTetrimino.currentPosition.y);
					mRefreshPreview = true;
                }
            }

			//Move piece to the right
			if (Input.GetKeyDown(mGameSettings.moveRightKey))
            {
                if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x + 1,
                                                  mCurrentTetrimino.currentPosition.y,
                                                  mCurrentTetrimino,
                                                  mCurrentTetrimino.currentRotation))
                {
                    mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x + 1, mCurrentTetrimino.currentPosition.y);
					mRefreshPreview = true;
                }
            }

            //Make the piece fall faster
            //this is the only input with GetKey instead of GetKeyDown, because most of the time, users want to keep this button pressed and make the piece fall
			if (Input.GetKey(mGameSettings.moveDownKey)||moveDownfast)
            {
                if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
                                                  mCurrentTetrimino.currentPosition.y + 1,
                                                  mCurrentTetrimino,
				                                  mCurrentTetrimino.currentRotation))
                {
					mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x, mCurrentTetrimino.currentPosition.y + 1);
                }
                else
                {
					moveDownfast = false;
				}
            }

            //This part is responsable for rendering the preview piece in the right position
			if(mRefreshPreview)
			{
				var y = mCurrentTetrimino.currentPosition.y;
				while(mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
                                                  y,
                                                  mCurrentTetrimino,
                                                  mCurrentTetrimino.currentRotation))
				{
					y++;
				}

				mPreview.ForcePosition(mCurrentTetrimino.currentPosition.x, y - 1);
				mRefreshPreview = false;
			}
		}
		public void LeftDir()
		{
			if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x - 1,
												 mCurrentTetrimino.currentPosition.y,
												 mCurrentTetrimino,
												 mCurrentTetrimino.currentRotation))
			{
				mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x - 1, mCurrentTetrimino.currentPosition.y);
				mRefreshPreview = true;
			}
		}
		public void RighttDir()
		{
			if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x + 1,
												 mCurrentTetrimino.currentPosition.y,
												 mCurrentTetrimino,
												 mCurrentTetrimino.currentRotation))
			{
				mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x + 1, mCurrentTetrimino.currentPosition.y);
				mRefreshPreview = true;
			}
		}
		public void DownDir()
		{
			if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
												mCurrentTetrimino.currentPosition.y + 1,
												mCurrentTetrimino,
												mCurrentTetrimino.currentRotation))
			{
				mCurrentTetrimino.currentPosition = new Vector2Int(mCurrentTetrimino.currentPosition.x, mCurrentTetrimino.currentPosition.y + 1);
			}
		}
		public void LeftRotate()
		{
			if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
												 mCurrentTetrimino.currentPosition.y,
												 mCurrentTetrimino,
												 mCurrentTetrimino.PreviousRotation))
			{
				mCurrentTetrimino.currentRotation = mCurrentTetrimino.PreviousRotation;
				mRefreshPreview = true;
			}
		}
		public void Rightrotate()
		{
			if (mPlayfield.IsPossibleMovement(mCurrentTetrimino.currentPosition.x,
												  mCurrentTetrimino.currentPosition.y,
												  mCurrentTetrimino,
												  mCurrentTetrimino.NextRotation))
			{
				mCurrentTetrimino.currentRotation = mCurrentTetrimino.NextRotation;
				mRefreshPreview = true;
			}
		}
	}
}
