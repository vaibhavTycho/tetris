﻿using System.Collections.Generic;

namespace TetrisEngine.TetriminosPiece
{
	//Class responsable for generating random pieces
	//if mControledRandom is true, it makes sure no piece is choosen twice befere all other types are choosen
    //if not, a random type is choosen
    public class TetriminoSpawner_Tetris
	{
		private List<TetriminoSpecs_Tetris> mAllTetriminos = new List<TetriminoSpecs_Tetris>();
		private List<TetriminoSpecs_Tetris> mAvailableTetriminos = new List<TetriminoSpecs_Tetris>();

		private bool mControledRandom;

		public TetriminoSpawner_Tetris(bool controledRandom, List<TetriminoSpecs_Tetris> allTetriminos)      
		{
			mAllTetriminos = allTetriminos;
			mControledRandom = controledRandom;
		}

		public Tetrimino_Tetris GetRandomTetrimino()
		{
			if (mControledRandom)
			{
				//if the list is empty, it creates a new one with all the tetriminos inside the project and chooses one to return
				if (mAvailableTetriminos.Count == 0)
					mAvailableTetriminos = GetFullTetriminoBaseList();

				var tetriminoSpecs = mAvailableTetriminos[RandomGenerator_Tetris.random.Next(0, mAvailableTetriminos.Count)];
				mAvailableTetriminos.Remove(tetriminoSpecs);
				return new Tetrimino_Tetris(tetriminoSpecs);
			}
                    
            //creates an instance a random object
			return new Tetrimino_Tetris(mAllTetriminos[RandomGenerator_Tetris.random.Next(0, mAllTetriminos.Count)]);
		}

		private List<TetriminoSpecs_Tetris> GetFullTetriminoBaseList()
		{
			var allTetriminos = new List<TetriminoSpecs_Tetris>(mAllTetriminos);
			return allTetriminos;
		}
    }
}
