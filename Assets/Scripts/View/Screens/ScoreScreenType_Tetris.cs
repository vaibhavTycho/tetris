﻿using UnityEngine;
using UnityEngine.UI;

public abstract class ScoreScreenType_Tetris<T> : BaseScreen_Tetris<T> where T : Component
{
	[SerializeField]
    protected Text scoreText, highscoreText;
  
	protected string mScorePrefix = "SCORE ";
    protected string mhighscorePrefix = "HIGHSCORE ";
	 
    protected void SetScoreText(int value)
    {
        scoreText.text = mScorePrefix + value;
        if(value>=PlayerPrefs.GetInt("highscore",0))
        {
            PlayerPrefs.SetInt("highscore", value);
            SetHighscoreText(value);
        }
    }
    protected void SetHighscoreText(int value)
    {
        highscoreText.text = mhighscorePrefix + value;
    }
}
