﻿using UnityEngine;

public class GameOver_Tetris : ScoreScreenType_Tetris<GameOver_Tetris> {

	public override void ShowScreen(float timeToTween = TIME_TO_TWEEN)
	{
		SetScoreText(Score_Tetris.instance.PlayerScore);
		base.ShowScreen(timeToTween);
		Debug.Log("GAmeover panel open  "+timeToTween);
	}
}
