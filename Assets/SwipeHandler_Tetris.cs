﻿using UnityEngine;
using System.Collections;
using TetrisEngine;
using System;

public class SwipeHandler_Tetris : MonoBehaviour
{

    public bool isSwiping;
    public static SwipeHandler_Tetris instance;
    public GameLogic_Tetris gameLogic_;
    public enum SwipeAxis { X, Y };
    public enum SwipeDirection { UP, DOWN, LEFT, RIGHT };

    public float sensibility,diffAllowed;
    DateTime d1, d2;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
       
    }
    private Vector2 startPos;
    // Update is called once per frame
    void Update()
    {
      
        if (Input.GetMouseButtonDown(0))
        {
            d1 = DateTime.Now;
            StopAllCoroutines();
            isSwiping = true;
            StartCoroutine(SwipeRoutine(Input.mousePosition));
            startPos = Input.mousePosition;
           

        }

        if (Input.GetMouseButtonUp(0))
        {
            SwipeAxis swipeAxis = DefineSwipeAxis(startPos, Input.mousePosition);
            SwipeDirection swipeDirection = DefineSwipeDirection(startPos, Input.mousePosition, swipeAxis);
            d2 = DateTime.Now;
            var d3 = (d2 - d1).TotalMilliseconds;
            gameLogic_.moveDownfast = ((d3 < 400)&& swipeDirection == SwipeDirection.DOWN) ? true : false;
            StopAllCoroutines();
            Approximately(startPos, Input.mousePosition, diffAllowed);
           
            isSwiping = false;
        }
    }
    private void Approximately(Vector2 start, Vector2 last, float alloweddiff)
    {
        var dx = start.x - last.x;
        if (Mathf.Abs(dx) <= alloweddiff)
        {
            gameLogic_.LeftRotate();
        }
        else
        {
            var dy = start.y - last.y;
            if (Mathf.Abs(dy) <= alloweddiff)
                gameLogic_.LeftRotate();

        }
    }
    public IEnumerator SwipeRoutine(Vector2 startPos)
    {
        
        while (isSwiping)
        {
            Vector2 currentTouchPoint = Input.mousePosition;

         //   Debug.Log(Vector2.Distance(startPos, currentTouchPoint));

            if (Vector2.Distance(startPos, currentTouchPoint) > sensibility)
            {                
                ConsumeSwipe(startPos, Input.mousePosition);
                startPos = Input.mousePosition;
            }
       
            yield return null;
        }

        yield return null;
    }


    public void ConsumeSwipe(Vector2 startPos, Vector2 endPos)
    {
        SwipeAxis swipeAxis = DefineSwipeAxis(startPos, endPos);
        SwipeDirection swipeDirection = DefineSwipeDirection(startPos,endPos, swipeAxis);

        if(swipeDirection == SwipeDirection.RIGHT)
        {
            gameLogic_.RighttDir();
        }

        if (swipeDirection == SwipeDirection.LEFT)
        {
            gameLogic_.LeftDir();
        }

        if (swipeDirection == SwipeDirection.UP)
        {
        }

        if (swipeDirection == SwipeDirection.DOWN)
        {
            gameLogic_.DownDir();
        }


      //  Debug.Log(swipeDirection);
    }

    public SwipeAxis DefineSwipeAxis(Vector2 startPos, Vector2 endPos)
    {
        Vector2 difference = startPos - endPos;
        float distanceInX = Mathf.Abs(difference.x);
        float distanceInY = Mathf.Abs(difference.y);

        if (distanceInX > distanceInY)
        {
            return SwipeAxis.X;
        }
        else
        {
            return SwipeAxis.Y;
        }
    }

    public SwipeDirection DefineSwipeDirection(Vector2 startPos, Vector2 endPos, SwipeAxis swipeAxis)
    {


        if (swipeAxis == SwipeAxis.X)
        {
            if (startPos.x > endPos.x)
            {
                return SwipeDirection.LEFT;
            }
            else
            {
                return SwipeDirection.RIGHT;
            }
        }
        else
        {
            if (startPos.y > endPos.y)
            {
                return SwipeDirection.DOWN;
            }
            else
            {
                return SwipeDirection.UP;
            }
        }
    }


}
